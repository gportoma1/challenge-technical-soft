package br.com.guilherme.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthModel {
    private String username;
    private String password;
}
