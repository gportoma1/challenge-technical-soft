package br.com.guilherme.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductModel {
    private String title;
    private String description;
    private int price;
    private double discountPercentage;
    private double rating;
    private int stock;
    private String brand;
    private String category;
    private String thumbnail;
}
