package br.com.guilherme.request;

import br.com.guilherme.core.BaseRequest;
import br.com.guilherme.model.ProductModel;
import br.com.guilherme.util.RandomUtils;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static br.com.guilherme.util.RandomUtils.randomElement;

public class ProductRequest extends BaseRequest {

    private final ProductModel productBody = new ProductModel();

    public void getProductsWithAuthentication() {
        response = request
                .header("Authorization", "Bearer " + createTokenForAuthenticationRequest(user, pwd).path("token").toString())
                .when()
                .get("/auth/products")
                .then()
                .extract().response();
    }

    public void getProductsWithAnInvalidAuthentication() {
        response = request
                .header("Authorization", "Bearer 12345678")
                .when()
                .get("/auth/products")
                .then()
                .extract().response();
    }

    public void getForTokenFreeProducts() {
        response = request
                .when()
                .get("/auth/products")
                .then()
                .extract().response();
    }

    public void createProduct() {
        makeBodyProduct();
        response = request
                .contentType(ContentType.JSON)
                .when()
                .body(productBody)
                .post("/products/add")
                .then().log().all()
                .extract().response();
    }

    public void getAllProducts() {
        response = request
                .when()
                .get("/products")
                .then().log().all()
                .extract().response();
    }

    public void getProductByIdExistent() {
        response = getProductById(RandomUtils.generateRandomIndex(30));
    }

    public void getForNonExistentProduct() {
        response = getProductById(12345);
    }

    private Response getProductById(int id) {
        return request
                .pathParam("id", id)
                .when()
                .get("/products/{id}")
                .then()
                .extract()
                .response();
    }

    private void makeBodyProduct() {
        productBody.setTitle(randomElement(RandomUtils.TITLES));
        productBody.setDescription(randomElement(RandomUtils.DESCRIPTIONS));
        productBody.setPrice((int) (RandomUtils.random.nextDouble() * 100));
        productBody.setDiscountPercentage(RandomUtils.random.nextDouble() * 10);
        productBody.setRating(RandomUtils.random.nextDouble() * 5);
        productBody.setStock(RandomUtils.random.nextInt(100));
        productBody.setBrand(RandomUtils.randomString(5));
        productBody.setCategory(randomElement(RandomUtils.CATEGORIES));
        productBody.setThumbnail(RandomUtils.randomString(15));
    }

    public void testResponseContainsArrayOfProducts() {
        assertResponseContainsField("products");
    }

    public void testResponseContainsFieldsTotalSkipAndLimit() {
        assertResponseContainsField("total");
        assertResponseContainsField("skip");
        assertResponseContainsField("limit");
    }

    public void testResponseHasExpectedProperties() {
        assertResponseContainsProperty("id");
        assertResponseContainsProperty("title");
        assertResponseContainsProperty("description");
        assertResponseContainsProperty("price");
        assertResponseContainsProperty("discountPercentage");
        assertResponseContainsProperty("rating");
        assertResponseContainsProperty("stock");
        assertResponseContainsProperty("brand");
        assertResponseContainsProperty("category");
        assertResponseContainsProperty("thumbnail");
        assertResponseContainsProperty("images");
    }

    public void testResponseHasExpectedPropertiesCreate() {
        assertResponseContainsProperty("id");
        assertResponseContainsProperty("title");
        assertResponseContainsProperty("price");
        assertResponseContainsProperty("discountPercentage");
        assertResponseContainsProperty("stock");
        assertResponseContainsProperty("rating");
        assertResponseContainsProperty("thumbnail");
        assertResponseContainsProperty("description");
        assertResponseContainsProperty("brand");
        assertResponseContainsProperty("category");
    }

    public void testResponseContainsFieldsNameAndMessage() {
        assertResponseContainsField("name");
        assertResponseContainsField("message");
    }

    public void testAttributesNameAndMessageContainsJsonWebTokenErrorAndInvalidExpiredToken() {
        assertMessageContains("name", "JsonWebTokenError");
        assertMessageContains("message", "Invalid/Expired Token!");
    }

    public void testResponseContainsFieldMessage() {
        assertResponseContainsField("message");
    }

    public void testAttributeMessageContainsAuthenticationProblem() {
        assertMessageContains("message", "Authentication Problem");
    }

    public void testAttributemessageContainsProductWithIdAndNotFound() {
        assertMessageContains("message", "Product with id");
        assertMessageContains("message", "not found");
    }
}
