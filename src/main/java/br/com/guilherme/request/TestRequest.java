package br.com.guilherme.request;

import br.com.guilherme.core.BaseRequest;

public class TestRequest extends BaseRequest {

    public void getStatusForApplication() {
        response = request
                .when()
                .get("/test")
                .then()
                .extract().response();
    }

    public void testResponseContainsFieldsStatusAndMethod() {
        assertResponseContainsField("status");
        assertResponseContainsField("method");
    }

}
