package br.com.guilherme.request;

import br.com.guilherme.core.BaseRequest;
import br.com.guilherme.util.RandomUtils;

public class AuthRequest extends BaseRequest {

    public void getUsersForAuthentication() {
        response = request
                .when()
                .get("/users")
                .then()
                .extract().response();
        index = RandomUtils.generateRandomIndex(response.path("limit"));
        user = response.path("users[" + index + "].username").toString();
        pwd = response.path("users[" + index + "].password").toString();
    }

    public void createTokenForAuthentication() {
        response = createTokenForAuthenticationRequest(user, pwd);
    }

    public void createTokenWithInvalidCredentials() {
        response = createTokenWithInvalidCredentialsRequest();
    }

    public void testUsersArrayIsPresentAndNotEmpty() {
        assertResponseArrayIsPresentAndNotEmpty();
    }

    public void testTotalFieldIsNonNegativeInteger() {
        assertResponseFieldIsNonNegativeInteger("total");
    }

    public void testRequiredFieldsPresence() {
        assertResponseHasRequiredFields("id", "username", "email", "firstName", "lastName", "gender", "image", "token");
    }

    public void testIdNonNegativeInteger() {
        assertResponseFieldIsNonNegativeInteger("id");
    }

    public void testEmailFormat() {
        assertResponseFieldMatchesRegex();
    }

    public void testResponseContainsFieldMessage() {
        assertResponseContainsField("message");
    }

    public void testAttributeMessageContainsMessageInvalidCredentials() {
        assertMessageContains("message", "Invalid credentials");
    }
}
