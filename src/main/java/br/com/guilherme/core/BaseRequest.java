package br.com.guilherme.core;

import br.com.guilherme.model.AuthModel;
import br.com.guilherme.util.ExtentManager;
import com.aventstack.extentreports.ExtentTest;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BaseRequest {

    protected static ExtentTest test;
    public final RequestSpecification request = given().log().all();
    public static Response response;
    public String user;
    public String pwd;
    public int index;
    private final AuthModel authBody = new AuthModel();

    public void createTest(String name, String description) {
        test = ExtentManager.createTest(name, description);
    }

    public void logInfo(String message) {
        test.info(message);
    }

    public void logPass(String message) {
        test.pass(message);
    }

    public void logFail(String message) {
        test.fail(message);
    }

    public void logResponseDetails() {

        test.info("Response Status Code: <pre>" + response.getStatusCode() + " </pre>");
        Map<String, String> responseHeaders = response.getHeaders().asList().stream()
                .collect(Collectors.toMap(Header::getName, Header::getValue));
        logInfo("Response Headers: <pre style='overflow-y: scroll; max-height: 150px;'>" + responseHeaders + " </pre>");
        logInfo("Response Content Type: <pre style='overflow-y: scroll; max-height: 150px;'>" + response.getContentType() + " </pre>");

        if (!response.getBody().asString().isEmpty()) {
            logInfo("Response Body: <pre style='overflow-y: scroll; max-height: 150px;'>" + response.getBody().prettyPrint() + " </pre>");
        }
    }

    public Response createTokenForAuthenticationRequest(String username, String password) {
        makeBodyAuth(username, password);
        return request
                .contentType(ContentType.JSON)
                .when()
                .body(authBody)
                .post("/auth/login")
                .then().log().all()
                .extract().response();
    }

    public Response createTokenWithInvalidCredentialsRequest() {
        return createTokenForAuthenticationRequest("abc", "123");
    }

    private void makeBodyAuth(String user, String pwd) {
        String defaultUser = (user != null) ? user : "kminchelle";
        String defaultPwd = (pwd != null) ? pwd : "0lelplR";

        authBody.setUsername(defaultUser);
        authBody.setPassword(defaultPwd);
    }

    public void assertResponseStatusCode(int status) {
        try {
            int statusCode = response.getStatusCode();
            assertEquals(status, statusCode);
            logPass("Response status code is " + status);
        } catch (AssertionError e) {
            logFail("Response status code is not " + status + ": " + e.getMessage());
            throw e;
        }
    }

    protected void assertResponseContainsField(String fieldName) {
        try {
            String responseBody = response.getBody().asString();
            assertTrue(responseBody.contains(fieldName), "Response has the required field - " + fieldName);
            logPass("Response has the required field - " + fieldName);
        } catch (AssertionError e) {
            logFail("Required field " + fieldName + " is missing: " + e.getMessage());
            throw e;
        }
    }

    protected void assertResponseContainsProperty(String propertyName) {
        try {
            String responseBody = response.getBody().asString();
            assertTrue(responseBody.contains("\"" + propertyName + "\":"),
                    "Response has the expected property - " + propertyName);
            logPass("Response has the expected property - " + propertyName);
        } catch (AssertionError e) {
            logFail("Required property " + propertyName + " is missing: " + e.getMessage());
            throw e;
        }
    }

    public void assertResponseContentType(String expectedContentType) {
        try {
            String contentType = response.contentType();
            assertTrue(contentType.contains(expectedContentType),
                    "Content-Type is not " + expectedContentType + ", found: " + contentType);
            logPass("Content-Type is " + expectedContentType);
        } catch (AssertionError e) {
            logFail("Content-Type is not " + expectedContentType + ": " + e.getMessage());
            throw e;
        }
    }

    protected void assertResponseArrayIsPresentAndNotEmpty() {
        try {
            Object responseData = response.getBody().as(Object.class);
            assertTrue(responseData instanceof java.util.Map, "Response data should be an object");

            java.util.Map<String, Object> responseObject = (java.util.Map<String, Object>) responseData;
            assertNotNull(responseObject.get("users"), "users" + " is missing in the response");
            assertTrue(responseObject.get("users") instanceof java.util.List, "users" + " is not a list");
            assertTrue(((java.util.List<?>) responseObject.get("users")).size() > 0,
                    "users" + " is empty in the response");
            logPass("The '" + "users" + "' array is present and not empty");
        } catch (AssertionError e) {
            logFail("The '" + "users" + "' array is not present or is empty: " + e.getMessage());
            throw e;
        }
    }

    protected void assertResponseFieldIsNonNegativeInteger(String fieldName) {
        try {
            Object responseData = response.getBody().as(Object.class);
            java.util.Map<String, Object> responseObject = (java.util.Map<String, Object>) responseData;

            assertTrue(responseObject.containsKey(fieldName), fieldName + " is missing in the response");
            Object fieldValue = responseObject.get(fieldName);

            assertTrue(fieldValue instanceof Number, fieldName + " value should be a number");
            assertTrue(((Number) fieldValue).intValue() >= 0, fieldName + " should be a non-negative integer");

            logPass(fieldName + " is a non-negative integer");
        } catch (AssertionError e) {
            logFail(fieldName + " is not a non-negative integer: " + e.getMessage());
            throw e;
        }
    }

    protected void assertResponseHasRequiredFields(String... fieldNames) {
        try {
            Object responseData = response.getBody().as(Object.class);
            assertTrue(responseData instanceof java.util.Map, "Response data should be an object");

            java.util.Map<String, Object> responseObject = (java.util.Map<String, Object>) responseData;

            for (String fieldName : fieldNames) {
                assertNotNull(responseObject.get(fieldName), fieldName + " is missing in the response");
            }

            logPass("Required fields are present in the response");
        } catch (AssertionError e) {
            logFail("Required fields are missing in the response: " + e.getMessage());
            throw e;
        }
    }

    protected void assertResponseFieldMatchesRegex() {
        try {
            Object responseData = response.getBody().as(Object.class);
            java.util.Map<String, Object> responseObject = (java.util.Map<String, Object>) responseData;

            assertTrue(responseObject.containsKey("email"), "email" + " is missing in the response");
            Object fieldValue = responseObject.get("email");

            assertTrue(fieldValue instanceof String, "email" + " value should be a string");

            String fieldValueStr = (String) fieldValue;

            assertTrue(fieldValueStr.matches("^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$"), "email" + " does not match the regex pattern: " + "^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$");

            logPass("email" + " matches the regex pattern: " + "^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$");
        } catch (AssertionError e) {
            logFail("email" + " does not match the regex pattern: " + e.getMessage());
            throw e;
        }
    }

    protected void assertMessageContains(String attribute, String expectedMessage) {
        try {
            String property = response.jsonPath().getString(attribute);
            assertTrue(property.contains(expectedMessage),
                    attribute + " attribute contains '" + expectedMessage + "'");
            logPass(attribute + " attribute contains '" + expectedMessage + "'");
        } catch (AssertionError e) {
            System.out.println(attribute + " attribute does not contain '" + expectedMessage + "': " + e.getMessage());
            throw e;
        }
    }
}
