package br.com.guilherme.util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentManager {
    private static ExtentReports extent;

    static {
        initializeExtentReport("Report/TestReport.html");
    }

    public static ExtentReports getExtentReport() {
        return extent;
    }

    public static void initializeExtentReport(String filePath) {
        ExtentSparkReporter htmlReporter = new ExtentSparkReporter(filePath);
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
    }

    public static ExtentTest createTest(String testName, String description) {
        return extent.createTest(testName, description);
    }

    public static void flushReport() {
        extent.flush();
    }
}
