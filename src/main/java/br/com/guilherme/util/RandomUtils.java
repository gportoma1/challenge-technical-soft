package br.com.guilherme.util;

import java.util.Random;

public class RandomUtils {
    public static final Random random = new Random();
    public static final String[] TITLES = {"Product A", "Product B", "Product C", "Product D"};
    public static final String[] DESCRIPTIONS = {"High quality product", "Affordable and durable", "Limited edition item"};
    public static final String[] CATEGORIES = {"Electronics", "Clothing", "Home & Garden", "Sports & Outdoors"};

    public static int generateRandomIndex(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Size must be a positive integer");
        }
        return random.nextInt(size);
    }

    public static String randomString(int length) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(chars.length());
            stringBuilder.append(chars.charAt(index));
        }
        return stringBuilder.toString();
    }

    public static String randomElement(String[] array) {
        int index = random.nextInt(array.length);
        return array[index];
    }
}
