package br.com.guilherme.runner;

import br.com.guilherme.core.RestAssuredConfig;
import br.com.guilherme.request.AuthRequest;
import br.com.guilherme.request.ProductRequest;
import br.com.guilherme.request.TestRequest;
import br.com.guilherme.util.YAMLConfigReader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import br.com.guilherme.util.ExtentManager;

public class ApiRequestTests {

    final TestRequest testRequest = new TestRequest();
    final AuthRequest authRequest = new AuthRequest();
    final ProductRequest productRequest = new ProductRequest();

    @BeforeAll
    public static void setUp() {
        RestAssuredConfig.setBaseURI(YAMLConfigReader.getValueByKey("base_url"));
    }

    @AfterAll
    public static void teardown() {
        ExtentManager.flushReport();
    }

    @Test
    public void getStatusForApplicationRun() {
        testRequest.createTest("Get Status for Application", "This test for validation if application is online");
        testRequest.getStatusForApplication();
        testRequest.assertResponseStatusCode(200);
        testRequest.assertResponseContentType("application/json");
        testRequest.testResponseContainsFieldsStatusAndMethod();
        testRequest.logResponseDetails();
    }

    @Test
    public void getUsersForAuthenticationRun() {
        authRequest.createTest("Get Users for Authentication", "This test for get users in application");
        authRequest.getUsersForAuthentication();
        authRequest.assertResponseStatusCode(200);
        authRequest.assertResponseContentType("application/json");
        authRequest.testUsersArrayIsPresentAndNotEmpty();
        authRequest.testTotalFieldIsNonNegativeInteger();
        authRequest.logResponseDetails();
    }

    @Test
    public void createTokenForAuthenticationRun() {
        authRequest.createTest("Create Token for Authentication", "This test for create token in application for authentication");
        authRequest.createTokenForAuthentication();
        authRequest.assertResponseStatusCode(200);
        authRequest.assertResponseContentType("application/json");
        authRequest.testRequiredFieldsPresence();
        authRequest.testIdNonNegativeInteger();
        authRequest.testEmailFormat();
        authRequest.logResponseDetails();
    }

    @Test
    public void tokenCreationWithInvalidCredentialsRun() {
        authRequest.createTest("Token creation with invalid credentials", "This test for create token in application for authentication");
        authRequest.createTokenWithInvalidCredentials();
        authRequest.assertResponseStatusCode(400);
        authRequest.assertResponseContentType("application/json");
        authRequest.testResponseContainsFieldMessage();
        authRequest.testAttributeMessageContainsMessageInvalidCredentials();
        authRequest.logResponseDetails();
    }

    @Test
    public void getProductsWithAuthenticationRun() {
        productRequest.createTest("Get Products With Authentication", "This test for get products with token of authentication");
        productRequest.getProductsWithAuthentication();
        productRequest.assertResponseStatusCode(200);
        productRequest.assertResponseContentType("application/json");
        productRequest.testResponseHasExpectedProperties();
        productRequest.testResponseContainsFieldsTotalSkipAndLimit();
        productRequest.logResponseDetails();
    }

    @Test
    public void getForProductsWithAnInvalidTokenRun() {
        productRequest.createTest("Get For Products With An Invalid Token", "This test for get for products with an invalid token");
        productRequest.getProductsWithAnInvalidAuthentication();
        productRequest.assertResponseStatusCode(401);
        productRequest.assertResponseContentType("application/json");
        productRequest.testResponseContainsFieldsNameAndMessage();
        productRequest.testAttributesNameAndMessageContainsJsonWebTokenErrorAndInvalidExpiredToken();
        productRequest.logResponseDetails();
    }

    @Test
    public void getForTokenFreeProductsRun() {
        productRequest.createTest("Get For Token-Free Products", "This test for get for token-free products");
        productRequest.getForTokenFreeProducts();
        productRequest.assertResponseStatusCode(403);
        productRequest.assertResponseContentType("application/json");
        productRequest.testResponseContainsFieldMessage();
        productRequest.testAttributeMessageContainsAuthenticationProblem();
        productRequest.logResponseDetails();
    }

    @Test
    public void createProductRun() {
        productRequest.createTest("Create Product", "This test for create product");
        productRequest.createProduct();
        productRequest.assertResponseStatusCode(200);
        productRequest.assertResponseContentType("application/json");
        productRequest.testResponseHasExpectedPropertiesCreate();
        productRequest.logResponseDetails();
    }

    @Test
    public void getAllProductsRun() {
        productRequest.createTest("Get All Products", "This test for get all products");
        productRequest.getAllProducts();
        productRequest.assertResponseStatusCode(200);
        productRequest.assertResponseContentType("application/json");
        productRequest.testResponseContainsArrayOfProducts();
        productRequest.testResponseContainsFieldsTotalSkipAndLimit();
        productRequest.logResponseDetails();
    }

    @Test
    public void getProductByIdRun() {
        productRequest.createTest("Get Product By ID", "This test for get product by ID");
        productRequest.getProductByIdExistent();
        productRequest.assertResponseStatusCode(200);
        productRequest.assertResponseContentType("application/json");
        productRequest.testResponseHasExpectedProperties();
        productRequest.logResponseDetails();
    }

    @Test
    public void getForNonExistentProducRun() {
        productRequest.createTest("Get For Non-existent Product", "This test for get for non-existent product");
        productRequest.getForNonExistentProduct();
        productRequest.assertResponseStatusCode(404);
        productRequest.assertResponseContentType("application/json");
        productRequest.testResponseContainsFieldMessage();
        productRequest.testAttributemessageContainsProductWithIdAndNotFound();
        productRequest.logResponseDetails();
    }
}
