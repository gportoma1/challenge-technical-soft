# Challenge Technical Soft

Este projeto de automação foi desenvolvido utilizando Java, Rest Assured, Gradle e Extent Reports. Ele tem como objetivo automatizar testes de API, fornecendo uma estrutura organizada para execução e relatórios detalhados.

#### Como Executar:

1. Certifique-se de ter o Java instalado em sua máquina.
2. Clone este repositório em seu ambiente local.
3. Navegue até o diretório raiz do projeto.
4. Execute o seguinte comando para executar os testes:

```bash
./gradlew test
```

5. Os relatórios serão gerados automaticamente na pasta `Report`.

#### Plano de Teste:

````
1. Teste de Status da Aplicação:
    - Verifica se a aplicação está online.
    - Método de Teste: `getStatusForApplication`

2. Teste de Usuários para Autenticação:
    - Verifica se os usuários podem ser recuperados na aplicação.
    - Método de Teste: `getUsersForAuthentication`

3. Teste de Criação de Token para Autenticação:
    - Verifica se o token pode ser criado na aplicação para fins de autenticação.
    - Método de Teste: `createTokenForAuthentication`

4. Teste de Criação de Token com Credenciais Inválidas:
    - Verifica se um token não pode ser criado com credenciais inválidas.
    - Método de Teste: `tokenCreationWithInvalidCredentials`

5. Teste de Obtenção de Produtos com Autenticação:
    - Verifica se os produtos podem ser obtidos com um token de autenticação válido.
    - Método de Teste: `getProductsWithAuthentication`

6. Teste de Obtenção de Produtos com um Token Inválido:
    - Verifica se os produtos não podem ser obtidos com um token inválido.
    - Método de Teste: `getForProductsWithAnInvalidToken`

7. Teste de Obtenção de Produtos sem Token:
    - Verifica se os produtos não podem ser obtidos sem um token.
    - Método de Teste: `getForTokenFreeProducts`

8. Teste de Criação de Produto:
    - Verifica se um produto pode ser criado na aplicação.
    - Método de Teste: `createProduct`

9. Teste de Obtenção de Todos os Produtos:
    - Verifica se todos os produtos podem ser obtidos na aplicação.
    - Método de Teste: `getAllProducts`

10. Teste de Obtenção de Produto por ID:
    - Verifica se um produto pode ser obtido na aplicação com base em seu ID.
    - Método de Teste: `getProductById`

11. Teste de Obtenção de Produto Inexistente:
    - Verifica se um produto inexistente não pode ser obtido na aplicação.
    - Método de Teste: `getForNonExistentProduct
````

Este plano de teste abrange os principais casos de uso da aplicação e visa garantir sua funcionalidade e integridade.

#### Estrutura do Projeto:

````
├── Report
└── src
    └── main
        ├── java
        │   └── br.com.guilherme
        │       ├── core
        │       ├── model
        │       ├── request
        │       ├── runner
        │       └── util
        └── resources
````

- **Report**: Contém os relatórios das execuções com detalhes.
- **src/main/java/br.com.guilherme/core**: Contém o núcleo da aplicação, incluindo configurações do Rest Assured e uma requisição base para outras requisições.
- **src/main/java/br.com.guilherme/model**: Classes Java utilizando POJO.
- **src/main/java/br.com.guilherme/request**: Classes com as requisições e regras de negócio, bem como seus testes.
- **src/main/java/br.com.guilherme/runner**: Classe responsável por iniciar a automação.
- **src/main/java/br.com.guilherme/util**: Classes utilitárias para o projeto.
- **src/main/java/resources**: Arquivo YAML para configurar o projeto.